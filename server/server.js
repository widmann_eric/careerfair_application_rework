var http = require('http');
var url = require('url');
var vm = require('vm');
var server = http.createServer(handler);
var io = require('socket.io')(server);
var fs = require('fs');


io.on('connection',function(socket){
	
	// socket.on('login',function(data){
	// 	adminLogin(data,socket.id);
	// });
	
	// socket.on('getEventList',function(data){
	// 	//getEventList(data,socket.id);
	// 	handleDataRequest('getEventList',data,socket.id);
	// });

	// socket.on('getEventData',function(data){
	// 	//getEventData(data,socket.id);
	// 	handleDataRequest('getEventData',data,socket.id);
	// });

	// socket.on('getApplicantData',function(data){
	// 	//getApplicantData(data,socket.id);
	// 	handleDataRequest('getApplicantData',data,socket.id);
	// });

	// socket.on('getEventTable',function(data){
	// 	handleDataRequest('getEventTable',data,socket.id);
	// });

	var onevent = socket.onevent;
	socket.onevent = function (packet) {
	    var args = packet.data || [];
	    onevent.call (this, packet);    // original call
	    packet.data = ["*"].concat(args);
	    onevent.call(this, packet);      // additional call to catch-all
	};

	socket.on('*',function(event,data){
		console.log(event);
		if(event.indexOf('get')!==-1)
			handleDataRequest(event,data,socket.id);
		else
			handlePostRequest(event,data,socket.id);
	});
});




var db = require('mysql');
var con = db.createConnection({
	host : 'internal-dev.cmdjf9dcmjok.us-east-1.rds.amazonaws.com',
	user : 	'careerfair_dev',
	password : 'cJKpQQPZpm3LMg9ZALQe',
	database : 'careerfair'
});
con.on('error',function(err){
	console.log('Fatal DB Error!');
	console.log(err);
	return;
});





server.listen(4040);

function handler(req,res){


	var args = {};
	
	if(req.method == 'GET'){
	
		var parsedURL = url.parse(req.url,true);
		args = parsedURL.query;
		
		//non-html request
		var tmpFile = getFile(req.url);
		if(tmpFile === 'nsf'){
			res.writeHead(400, {'Content-Type' : 'text/html' });
			res.end('Invalid location');
			return;
		}


		res.writeHead(200, {"Content-Type" : getType(req.url)});
		res.end(tmpFile);
		return;
	}
	

	else{
		res.writeHead(200, {'Content-Type': 'text/html'});
		res.write("Careerfair Server");
		res.end();
	}
}



function getFile(u){
	try{
	var tmpPath = '..' + u;
	return fs.readFileSync(tmpPath);
	}catch(ex){

		//console.log('No Such File!');
		return 'nsf';
	}
}

function getType(u){
	if(u.indexOf('/css/') !== -1)
		return 'text/css';
	else if (u.indexOf('/js/') !== -1)
		return 'text/js';
	else{
		return  'text/html';
	}
}


//_______________________________________________________________________SQL FUNCTIONS__________________________________________________________________

var loggedSockets = {};


function respondToClient(event, actualID,data){
	console.log("Sending ["+event+"]~["+actualID+"]");
	io.emit(event+loggedSockets[actualID],data);
}


function handleDataRequest(event,packet,actualID){
	loggedSockets[actualID] = packet.sok;
	var query = null; 
	var response = null;
	var bind = [];
	var callB = null;
	switch(event){
		case 'adminLogin' :
			bind = [packet.user,packet.password];
			query = 'CALL ADMIN_LOGIN( ? , ? );';
			callB = function(result,act){
					if(result[0][0].SUCCESS===1)
						respondToClient('loginSuccess',act,1);
		
					else
						respondToClient('loginSuccess',act,0);};
			break;

		case 'getEventList' :
			query = 'CALL ADMIN_EVENT_LIST()';
			response = 'eventList';
			break;

		case 'getEventData':
			query = 'CALL ADMIN_GET_DASHBOARD_DATA( ? )';
			response = 'eventData';
			bind = [packet.event];
			break;

		case 'getApplicantData':
			query = 'CALL ADMIN_GET_APPLICANT_DATA()';
			response = 'applicantData';
			break;

		case 'getEventTable':
			query ='CALL ADMIN_GET_EVENT_DATA()';
			response ='eventTable';
			break;
		
		case 'getQuestionData':
			query = 'CALL ADMIN_GET_QUESTION_DATA( ? , ? )';
			bind = [packet.event,packet.interviewQuestion];
			response = 'questionData';
			break;

		case 'getUserData':
			query = 'CALL ADMIN_GET_USER_DATA()';
			response = 'userData';
			break;
		
		default:
			return;
		}

		if(bind.length>0){
			con.query(query,bind,function(err,result){
				if(!callB){
					respondToClient(response,actualID,result);
				}
				else{
					callB(result,actualID);
				}
			});
		}

		else{
			con.query(query,function(err,result){
				respondToClient(response,actualID,result);
			});
		}	
}


function handlePostRequest(event,packet,actualID){
	loggedSockets[actualID] = packet.sok;
	var query = null; 
	var response = null;
	var bind = [];
	switch(event){
		case 'addEvent' :
			query = 'CALL ADMIN_NEW_EVENT( ? , ? , ? )';
			bind = [packet.name,packet.location,packet.date];
			response = 'addEventStatus';
			break;
		case 'addTextualQuestion':
			query ='CALL ADMIN_NEW_QUESTION( ? , ? , ? , ? , ? , ? )';
			bind =[packet.event,packet.order,packet.type,packet.weight,packet.question,packet.set];
			console.log(bind);
			response ='addQuestionStatus';
			break;
		case 'addAnswer':
			query ='CALL INSERT_MULTI_ANSWER( ? , ? , ? , ? )';
			bind = [packet.questionID,packet.text,packet.weight,packet.event];
			response = 'addAnswerStatus';
			break;
		default :
			return;
	}

	con.query(query,bind,function(err,result){

		respondToClient(response,actualID,result);
	});
}





