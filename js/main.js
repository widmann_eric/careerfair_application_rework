
var nodeServerIP = 'http://192.168.24.64:4040';
var scriptUrl  = nodeServerIP+'/socket.io/socket.io.js';
var socket = '';
var socketID = 0;
var verified = false;


var eventList;
var eventData;
var participantData;



jQuery.getScript(scriptUrl,function (data,textStatus,jqxhr){
	socket = io(nodeServerIP);
	configureSocket();

});


function configureSocket(){

		//stall for connection
		if(socket.id === undefined){
			setTimeout(function(){
				configureSocket();
			}, 100);
		return;
		}

		socketID = socket.id;


		//_____________________________________________________GETS_________________________________________________
		socket.on('loginSuccess'+socketID,function(data){
			console.log(data);
			if(data == 1)
				loginSuccess();
			else
				loginFail();
		});
		socket.on('eventList'+socketID,function(data){
					var reducedResult = data[0];
					var finData = [];
					for(i in reducedResult){
						finData.push([reducedResult[i].id+': '+reducedResult[i].name]);
					}

			populateEventTable(finData);
		});


		socket.on('eventData'+socketID,function(data){
			participantData=data[0];
			fixData(data,['score','name','major','position','phone_number','email']);
		});
		
		socket.on('applicantData'+socketID,function(data){
			fixData(data,['name','major','grad_year','position','score','last_apply']);
		});

		socket.on('eventTable'+socketID,function(data){
			fixData(data,['name','location','date','Applicants']);
		});

		socket.on('questionData'+socketID,function(data){
			console.log(data);
			fixData(data,['_order','type','weight','text']);
		});		

		socket.on('userData'+socketID,function(data){
			console.log(data);
			fixData(data,['name','email','user']);
		});



		//______________________________________________POST___________________________________________________________

		socket.on('addEventStatus'+socketID,function(data){
			handleStatus(data[0][0].CREATED,'event');
		});

		socket.on('addQuestionStatus'+socketID, function(data){
			console.log(data);
			handleStatus(data[0][0].CREATED,'question');
		});

		socket.on('addAnswerStatus'+socketID,function(data){
			console.log('answer through');
		});

}


