var currPage = 'Dashboard';

function randomHsl() {
    return 'hsla(' + (Math.random() * 360) + ', 100%, 50%, 1)';
}

function initPage(){

	hideAddOnElements();

	//$('.mainHeader').css('background-color',randomHsl());
	refresh();
}



function setPage(page){
	$('.'+lowerFirstLetter(currPage)).css('background-color','white');
	currPage = page;
	refresh();
}


function refresh(){
	
	//if socket not yet connected
	if(socket == '' || socketID == 0){
		setTimeout(function(){
			refresh();
		},100);
		return;
	}


	$(document).ready(function(){

		hideAddOnElements();
		resetAddQuestion();
		

		

		//destroy old table if necessary
		if($.fn.DataTable.isDataTable(coreTable)){
			console.log('destorying table');
			coreTable.DataTable().clear();
			coreTable.DataTable().destroy();
			$('.coreTable th').remove();

		}
		
		if($.fn.DataTable.isDataTable('.eventTable')){
			$('.eventTable').DataTable().destroy();
			$('.eventTable').empty();
			$('.coreTable th').remove();
		}
		

		


		switch(currPage){
			case 'Dashboard':
				socket.emit('getEventList',{sok : socketID});
				socket.emit('getApplicantData',{sok: socketID, event : currEvent});
				$('.coreTable').DataTable({
						columns : dashboardColumns,
						sDom : 'Rfrtlip',
						pagingType:'simple',
						oLanguage:
							{sSearch : "<img src ='../assets/mag.png' class = 'outerEventMag'/>",
				 			sEmptyTable: 'No Data'}
					});
				socket.emit('getEventData',{sok: socketID, event: currEvent});
				searchEvent.show();
				$('.coreTable').on('click','tr',function(){
					populateUserModalInfo($('td',this).last().html());
				});
		
				break;
			
			case 'Applicants' :
				$('.coreTable').DataTable({
						columns : applicantColumns,
						sDom : 'Rfrtlip',
						pagingType:'simple',
						oLanguage:
							{sSearch : "<img src ='../assets/mag.png' class = 'outerEventMag'/>",
				 			sEmptyTable: 'No Data'}
					});
				socket.emit('getApplicantData',{sok : socketID});
				break;
			
			case 'Events':
				addEventButton.show();
				$('.coreTable').DataTable({
						columns : eventColumns,
						sDom : 'Rfrtlip',
						pagingType:'simple',
						oLanguage:
							{sSearch : "<img src ='../assets/mag.png' class = 'outerEventMag'/>",
				 			sEmptyTable: 'No Data'}
				});
				socket.emit('getEventTable',{sok : socketID});
				
				break;
			
			case 'Questions':
					addQuestionButton.show();
					$('.coreTable').DataTable({
						columns : questionColumns,
						sDom : 'Rfrtlip',
						pagingType:'simple',
						oLanguage:
							{sSearch : "<img src ='../assets/mag.png' class = 'outerEventMag'/>",
				 			sEmptyTable: 'No Data'}
					});
					socket.emit('getQuestionData', {sok : socketID, event : 0, interviewQuestion : 0});
					break;

			case 'Users' :
					$('.coreTable').DataTable({
						columns : userColumns,
						sDom : 'Rfrtlip',
						pagingType:'simple',
						oLanguage:
							{sSearch : "<img src ='../assets/mag.png' class = 'outerEventMag'/>",
				 			sEmptyTable: 'No Data'}
					});
					socket.emit('getUserData',{sok : socketID});
					break;
			
			default :
				break;
		}//end of switch


		
		$('.'+lowerFirstLetter(currPage)).css('background-color','rgba(218,196,196,.42)');
	

	});



}

function lowerFirstLetter(string) {
    return string.charAt(0).toLowerCase() + string.slice(1);
}


//____________________________________________________________TABLE POPULATION____________________________________________

function populateEventTable(data){
	$('.eventTable').DataTable(
		{
			columns:[{title: 'none'}],
			bPaginate : false,
			lengthChange : false,
			data:data,
			pageLength: 50,
			info : false,
			oLanguage:
				{sSearch : "<img src ='../assets/mag.png' class = 'eventMag'/>",
				 sEmptyTable: 'No Data'}
		}
	);
	$(document).ready(function(){
		$('.eventTable thead').hide();
		searchEvent.show();
		$('.eventContainer td').on('click',function(){
			var clickedEvent =  ($(this).html());
			clickedEvent = parseStringEvent(clickedEvent);
			filterEvent(clickedEvent);
		});
	});
}

function parseStringEvent(str){
	return str.substring(0, str.indexOf(':'));
}

function populateTable(data){
	var dataTab = coreTable.DataTable();
	dataTab.clear();
	dataTab.rows.add(data);
	dataTab.draw();
}



function fixData(data,propertyArray){
	var dataReduced = data[0];

	var tableINFO = [];
	for(i in dataReduced){
		var currRow = dataReduced[i];
		var tmp = [];
		for(j in propertyArray){
			if(!currRow[propertyArray[j]])
				currRow[propertyArray[j]] = '-';
			tmp.push(currRow[propertyArray[j]]);
		}
		tableINFO.push(tmp);
	}

	populateTable(tableINFO);
}


var currEvent = 0;
function filterEvent(event){
	console.log(event);
	currEvent = event;
	refresh();
}

//_____________________________________________________________SQL inserts_____________________________________________


function addEvent(){
	var name = addEventNameIn.val();
	var location = addEventLocIn.val();
	var date = addEventDateIn.val();
	console.log(date);
	if(name.length == 0 || location.length == 0 || date.length == 0){
		centerModalError.show();
		return;
	}
	else{
		socket.emit('addEvent',{sok:socketID,name:name,location:location,date:date});
		addEventContainer.hide();
		loadModal.fadeIn();
	}
	
}

function handleStatus(stat,mode){
	
	console.log(stat);

	if(stat){
		addAnswers(stat);
		loadModal.hide();
		resetAddQuestion();
		refresh();
	}
	else{
		loadModal.hide();

		if(mode =='event')
			addEventContainer.fadeIn();
		else if(mode =='question'){
			addQuestionContainer.fadeIn();
		}
		centerModalError.show();
	}
}




function addTextualQuestion(){
	var question = $('#textualAnswer').val();
	var weight = $('#textualWeight').val();
	var order = $('#textualOrder').val();
	var set = $('#textualSet option:selected').val();
	
	if(question.length == 0 || weight.length==0 || order.length==0 || set.length ==0){
		centerModalError.show();
	}

	else{
		if($('.addQuestionTypeSelect option:selected').val()=='text')
			socket.emit('addTextualQuestion',{sok:socketID,question:question,weight:weight,order:order,set:set,event:currEvent,type:'text'});
		else
			socket.emit('addTextualQuestion',{sok:socketID,question:question,weight:weight,order:order,set:set,event:currEvent,type:'range'});
		addQuestionContainer.hide();
		loadModal.fadeIn();
	}
}


function addMultiAnswer(){

	var question = $('#multiQuestion').val();
	var weight = $('#multiWeight').val();
	var order = $('#multiOrder').val();
	var set = $('#multiSet').val();
	socket.emit('addTextualQuestion',{sok:socketID,question:question,weight:weight,order:order,set:set,event:currEvent,type:'multi'});
	
}

var answerArray = [];
function addAnswers(id){
	answerArray = [];
	console.log($('.answers input').first().val());
	if($('.answers input').first().val()!=''){
		$('.answers input').each(function(){
			answerArray.push($(this).val());
		});
	}
	
	for(i in answerArray){
		socket.emit('addAnswer',{sok:socketID,event:currEvent,weight:0,text:answerArray[i],questionID:id});
	}

}





