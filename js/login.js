$(document).ready(function(){
	$(document).keypress(function(e){
		if(e.which==13)
			login();
	});

	$('#submit').on('click',function(e){
		login();
	});

	$('.loadModal').hide();
});

function login(){
		
		var loginPacket = 
		{ user : $('#username').val(),
		  password : $('#password').val(),
		 sok: socketID};

		 $('.loginFail').empty();
		 $('.loadModal').fadeIn();
		 $('.fields').hide();
		
		socket.emit('login',loginPacket);
}

function loginSuccess(){
	verified = true;
	console.log('login Success');
	window.location.replace(nodeServerIP+'/admin/dash.html');
}

function loginFail(){
	$('.fields').fadeIn();
	$('.loadModal').hide();
	$('.loginFail').append('Invalid Login!');
}