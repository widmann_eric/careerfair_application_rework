var coreTable;
var searchEvent;
var eventContainer;
var closeButtonEventSearch;
var loadModal;
var userViewModal;


var addEventContainer;
var addEventButton;
var addEventSubmit;
var addEventNameIn;
var addEventLocIn;
var centerModalError;
var addEventDateIn;
var closeButtonCenter;


var addQuestionContainer;
var addQuestionButton;
var addQuestionSubmit;
var addQuestionTypeSelect;
var addQuestionTextual;
var addQuestionTextualSubmit;
var addAnAnswer;


$(document).ready(function(){
	

	coreTable = $('.coreTable');
	eventContainer = $('.eventContainer');
	searchEvent = $('.searchEvent');
	loadModal = $('.loadModal');
	closeButtonEventSearch = $('.closeButton');
	userViewModal =  $('.userViewModal');

	searchEvent.on('click',function(){
		console.log('clicked');
		eventContainer.fadeIn();
		searchEvent.hide();
	});

	$('.tab').on('click',function(){
		setPage($(this).html());
	});

	closeButtonEventSearch.on('click',function(){
		eventContainer.hide();
		searchEvent.fadeIn();
	});


	 userViewModal.hide();

	//_____________________________________________________________ADD Event__________
	addEventContainer = $('.addEventContainer');
	addEventButton = $('.addEvent');
	addEventSubmit = $('.addEventContainer input[type=button]');
	addEventNameIn = $('#nameInEvent');
	addEventLocIn  = $('#locationInEvent');
	centerModalError =  $('.centerModalError');
	addEventDateIn = $('#dateInEvent');


	addEventButton.on('click',function(){
		addEventButton.hide();
		addEventContainer.fadeIn();
	});
	
	addEventSubmit.on('click', function(){
		addEvent();
		});

	
//____________________________________________________ADD QUESTION__________________________________________________________________________________________
	addQuestionButton = $('.addQuestion');
	addQuestionContainer = $('.addQuestionContainer');
	addQuestionSubmit = $('#addQuestionSubmit');
	addQuestionTypeSelect = $('.addQuestionTypeSelect');
	addQuestionMulti = $('.addQuestionMulti');
	addQuestionTextual = $('.addQuestionTextual');
	addQuestionTextualSubmit = $('#textualSubmit');
	addAnAnswer = $('#addAnAnswer');





	addQuestionMulti.hide();
	addQuestionTextual.hide();


	addQuestionButton.on('click',function(){
		addQuestionContainer.fadeIn();
		addQuestionButton.hide();
	});

	addQuestionTextualSubmit.on('click',function(){
		addTextualQuestion();
	});

	addQuestionSubmit.on('click',function(){
		var selected = $('.addQuestionTypeSelect option:selected').val();

		switch(selected){
			case 'multi':
				addQuestionMulti.fadeIn();
				break;
			case 'text':
				addQuestionTextual.fadeIn();
				break;
			case 'range':
				addQuestionTextual.fadeIn();
				break;
			default:
				break;
		}

		addQuestionTypeSelect.hide();
	});


	//____________________________________________________ QUESTION__________________________________________________________________________________________
	addAnAnswer.on('click',function(){
		var lastCharacter = $('.answers input[type=text]').last().attr('name');
		var newAscii = 97+Number.parseInt(lastCharacter);
		console.log(newAscii);
		var newString = String.fromCharCode(newAscii);
		$('.answers').append("<div>"+newString+".<input type='text' name = '"+(Number.parseInt(lastCharacter)+1)+"'></div>");
	});

	$('#multiSubmit').on('click',function(){
		addMultiAnswer();
	});



	closeButtonCenter = $('.closeButtonCenter');
	closeButtonCenter.on('click',function(){
		if(addEventContainer.is(':visible')){
			addEventContainer.hide();
			addEventButton.fadeIn();
		}
		else if (addQuestionContainer.is(':visible')){
			resetAddQuestion();
			addQuestionContainer.hide();
			addQuestionButton.fadeIn();
		}
		else
			userViewModal.fadeOut();
	});

		//____________________________________________________ QUESTION__________________________________________________________________________________________

	$('#userCloseButton').on('click',function(){
		userViewModal.hide();
	});

	


	initPage();
});

function resetAddQuestion(){
	$('.addQuestionTextual input[type=text], .addQuestionTextual input[type=number]').val('');
	$('.addQuestionMulti input[type=text],.addQuestionMulti input[type=number]').val('');
	addQuestionTextual.hide();
	addQuestionMulti.hide();
	addQuestionTypeSelect.show();

	$('.answers').empty();
	$('.answers').append('<div>a.<input type="text" name="1"></div>');
}


function hideAddOnElements(){
	eventContainer.hide();
	searchEvent.hide();
	addEventContainer.hide();
	addEventButton.hide();
	centerModalError.hide();
	addQuestionButton.hide();
	addQuestionContainer.hide();
	loadModal.hide();
}


function populateUserModalInfo(email){
		


		
		var currInfo = searchUser(email);
		console.log(currInfo);

		$('.userNameHead').empty();
		$('.userNameHead').append(currInfo.name);
		console.log(userData);

		for(i in userData){

			
			userData[i][1] = currInfo[userData[i][0].toLowerCase()];

			userData[i][0] = userData[i][0].toUpperCase();
		}
		console.log(userData);





		if($.fn.DataTable.isDataTable('#userTable')){
			$('#userTable').DataTable().destroy();
			$('#userTable').empty();
		}

		$('#userTable').DataTable({
			columns:[{title: 'none1234'},{title:'none1234'}],
			bPaginate : false,
			lengthChange : false,
			data:userData,
			pageLength: 50,
			info : false,
		});


	
		$('#userProfilePic').attr('src' , currInfo.profile_image);
		$('#resumeButton').on('click',function(){
			window.open(currInfo.resume_image,'_blank');
		});


	
		// $('#userTable tr').each(function(){
		// 	var color = Math.floor(Math.random()*16777215).toString(16);
		// 	$(this).css('background-color','rgba(252,252,240,.16)');
			
		// });

		$(document).ready(function(){
			userViewModal.fadeIn();
		});

}		


function searchUser(email){
	console.log(email);

	for(i in participantData){
		if(participantData[i].email==email)
			return participantData[i];
	}
}