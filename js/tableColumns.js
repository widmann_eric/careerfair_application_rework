var dashboardColumns = [
	{title: 'Score'},
	{title: 'Name'},
	{title : 'Major'},
	{title : 'Position'},
	{title: 'Phone'},
	{title: 'Email'}
];


var applicantColumns = [
	{title : 'Name'},
	{title : 'Major'},
	{title : 'Graduation Date'},
	{title : 'Position'},
	{title : 'Score'},
	{title : 'Last Applied'}

];

var eventColumns = [
	{title : 'Name'},
	{title : 'Location'},
	{title : 'Date'},
	{title : 'Applicants'}
];

var questionColumns = [
	{title : 'Order'},
	{title : 'Type'},
	{title : 'Weight'},
	{title : 'Question'}
];

var userColumns = [
	{title : 'Name'},
	{title : 'Email'},
	{title : 'Permissions'}
	
];


var userData =[
	['name'],
	['major'],
	['grad_term'],
	['grad_year'],
	['position'],
	['score'],
	['phone_number'],
	['email'],
	['id'],
	['previous_score']
];


